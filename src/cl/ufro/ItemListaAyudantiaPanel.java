package cl.ufro;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class ItemListaAyudantiaPanel extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_item_lista_ayudantia_panel);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.item_lista_ayudantia_panel, menu);
		return true;
	}

}
