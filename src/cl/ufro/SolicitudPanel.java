package cl.ufro;

import java.util.List;

import cl.ufro.bd.Asignatura;
import cl.ufro.bd.Ayudantia;
import cl.ufro.controller.AsignaturaController;
import cl.ufro.controller.AyudantiaController;
import cl.ufro.controller.UsuarioController;
import cl.ufro.service.AsignaturaService;
import android.os.Bundle;
import android.R.string;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class SolicitudPanel extends Activity {
	private Button spBtnSolicitarAyudantia;
	private EditText spTxtFecha;
	private EditText spTxtMateria;
	private EditText spTxtNpersonas;
	private EditText spTxtContenidos;
	private EditText spTxtPrecio;
	private String MateriaSelect;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_solicitud_panel);
		/*init*/
		AsignaturaController asignaturaController = new AsignaturaController();
		List <Asignatura> lista = asignaturaController.listar();
		
		String []asignaturas= new String[lista.size()];
		
		for(int i=0; i<lista.size();i++){
			Asignatura asignatura = lista.get(i);
			
			String asignaturaServer=asignatura.getNombre();

			asignaturas[i]=asignaturaServer;
		}

		Spinner spSpinnerAsignatura=(Spinner)findViewById(R.id.spSpinnerAsignatura);
		
		spSpinnerAsignatura.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, asignaturas));

		/**/



		spBtnSolicitarAyudantia = (Button)findViewById(R.id.spBtnSolicitar);
		spTxtFecha = (EditText)findViewById(R.id.spTxtFecha);
		spTxtMateria = (EditText)findViewById(R.id.spTxtAsignatura);
		spTxtNpersonas = (EditText)findViewById(R.id.spTxtNumpersonas);
		spTxtContenidos = (EditText)findViewById(R.id.spTxtContenido);
		spTxtPrecio = (EditText)findViewById(R.id.spTxtPrecio);

		spSpinnerAsignatura.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parent, View view,int pos, long id) {
				MateriaSelect= (String)parent.getItemAtPosition(pos);
				Toast.makeText(parent.getContext(), "Seleccionado:"+MateriaSelect, Toast.LENGTH_SHORT).show();	
				spTxtMateria.setText(MateriaSelect);
			}
			public void onNothingSelected(AdapterView<?> parent) {
			}
			});
		
		spBtnSolicitarAyudantia.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0){
				try{
					UsuarioController uContr =new UsuarioController();

					Ayudantia ayudantia = new Ayudantia();
					ayudantia.setFecha(spTxtFecha.getText().toString());
					ayudantia.setMateria(spTxtMateria.getText().toString());
					ayudantia.setnPersonas(spTxtNpersonas.getText().toString());
					ayudantia.setContenidos(spTxtContenidos.getText().toString());
					ayudantia.setMateria(MateriaSelect);
					ayudantia.setOferta(spTxtPrecio.getText().toString());
					ayudantia.setCorreoUsuario(uContr.getUsuarioSession().getCorreo());
					AyudantiaController ayudantiaController = new AyudantiaController();
					ayudantia = ayudantiaController.registrar(ayudantia);


					Log.e("BOTON", "Ayudantia Registrada con exito");

					Toast.makeText(SolicitudPanel.this, "Ayudantia Registrada con exito", Toast.LENGTH_SHORT).show();
				}catch (Exception ex){
					Log.e("ERROR", ex.getMessage());
				}
			}

		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.solicitud_panel, menu);
		return true;
	}

}
