package cl.ufro;

public class Item {
	 private String fecha;
	    private String precio;
	    private String NumPersonas;
	    private int id;
	    
	 
	    public Item() {
	        super();
	    }
	 
	    public Item(String fecha,String precio,String NumPersonas,int id){
	        super();
	        this.setNumPersonas(NumPersonas);
	        this.setFecha(fecha);
	        this.setPrecio(precio);
	        this.setId(id);
	    }

	    public void setId(int id){
	    	this.id = id;
	    }
	    public int getId(){
	    	return id;
	    }
		public void setFecha(String fecha) {
			this.fecha = fecha;
		}

		public String getFecha() {
			return fecha;
		}

		public void setPrecio(String precio) {
			this.precio = precio;
		}

		public String getPrecio() {
			return precio;
		}

		public void setNumPersonas(String NumPersonas) {
			this.NumPersonas = NumPersonas;
		}

		public String getNumPersonas() {
			return NumPersonas;
		}
	    
}
