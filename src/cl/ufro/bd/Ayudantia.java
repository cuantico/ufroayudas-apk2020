package cl.ufro.bd;

import java.io.Serializable;

import utils.ObjetoBd;


public class Ayudantia extends  ObjetoBd implements Serializable{

	private int id;
	private String oferta;
	private String fecha;
	private String materia;
	private String contenidos;
	private String correoUsuario;
	private String Ayudante;
	private String nPersonas;
	private int IdLastMsj;
	private boolean confirmar;
	public Ayudantia (){
		addToPrimaryKey("id");
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String  getOferta() {
		return oferta;
	}
	public void setOferta(String oferta) {
		this.oferta = oferta;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getMateria() {
		return materia;
	}
	public void setMateria(String materia) {
		this.materia = materia;
	}
	public String getContenidos() {
		return contenidos;
	}
	public void setContenidos(String contenidos) {
		this.contenidos = contenidos;
	}
	public String getCorreoUsuario() {
		return correoUsuario;
	}
	public void setCorreoUsuario(String correoUsuario) {
		this.correoUsuario = correoUsuario;
	}
	public String getnPersonas() {
		return nPersonas;
	}
	public void setnPersonas(String nPersonas) {
		this.nPersonas = nPersonas;
	}
	public void setConfirmar(boolean confirmar) {
		this.confirmar = confirmar;
	}
	public boolean isConfirmar() {
		return confirmar;
	}
	public void setAyudante(String ayudante) {
		this.Ayudante = ayudante;
	}
	public String getAyudante() {
		return Ayudante;
	}
	public void setIdLastMsj(int idLastMsj) {
		this.IdLastMsj = idLastMsj;
	}
	public int getIdLastMsj() {
		return IdLastMsj;
	}
	


}
