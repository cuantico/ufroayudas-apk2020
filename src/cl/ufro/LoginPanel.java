package cl.ufro;

import utils.ManagerSession;
import utils.Notificacion;
import cl.ufro.bd.Usuario;
import cl.ufro.controller.*;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginPanel extends Activity {
	private Button lpBtnSiguiente;
	private EditText lpTxtCorreo;
	private Notificacion noti;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_panel);
		lpBtnSiguiente = (Button)findViewById(R.id.lpBtnSiguiente);
		lpTxtCorreo = (EditText)findViewById(R.id.lpTxtCorreo);
		noti = new Notificacion(this);
		lpTxtCorreo.setText("");
		UsuarioController usuarioController = new UsuarioController();
		Usuario usuario = usuarioController.findFirst();
		//A U T O L O G I N
		if(usuario != null){
			usuario = usuarioController.identificar(usuario);
			Toast.makeText(LoginPanel.this, "1", Toast.LENGTH_SHORT).show();
		}
		if(usuario == null){
			Intent intenty = new Intent(LoginPanel.this,LoginPanel.class);
			Toast.makeText(LoginPanel.this, "", Toast.LENGTH_SHORT).show();

		}	
		else{
			//si el usuario se identifica correctamente accede al Home.
			ManagerSession.saveObject("usuario", usuario);
			Toast.makeText(LoginPanel.this, "Autologin SUCESS", Toast.LENGTH_SHORT).show();
			Intent intent02 = new Intent(LoginPanel.this,HomePanel.class);
			startActivity(intent02);
		}

		lpBtnSiguiente.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Usuario usuario = new Usuario();
				if(lpTxtCorreo.getText().toString().equals("")){
					Toast.makeText(LoginPanel.this, "Correo no valido", Toast.LENGTH_SHORT).show();
				}else{

				if(lpTxtCorreo.getText().toString()!="..."){
					usuario.setCorreo(lpTxtCorreo.getText().toString()+"@ufromail.cl");
					UsuarioController usuarioController = new UsuarioController();
					usuarioController.registrar(usuario);
					Toast.makeText(LoginPanel.this, "Pin enviado", Toast.LENGTH_SHORT).show();
					ManagerSession.saveObject("usuario", usuario);

					Intent intent1 = new Intent(LoginPanel.this,PinPanel.class);
					startActivity(intent1);

				}
			}
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login_panel, menu);
		return true;
	}

}
