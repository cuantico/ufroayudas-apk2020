package cl.ufro;


import cl.ufro.service.MyService;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class HomePanel extends Activity {
	private Button hpBtnSolicitar;
	private Button hpBtnVerAyudantias;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_panel);
		hpBtnSolicitar = (Button) findViewById(R.id.hpBtnSolicitar);
		hpBtnVerAyudantias = (Button) findViewById(R.id.hpBtnVerAyudantias);
	
		hpBtnVerAyudantias.setOnClickListener(new OnClickListener() {
	
			@Override
			public void onClick(View arg0) {
				try{
					startService(new Intent(HomePanel.this, MyService.class));
				}catch (Exception ex){
					Log.e("ERROR", ex.getMessage());
				}
				// TODO Auto-generated method stub
				Intent intentListado = new Intent(HomePanel.this,ListadoPanel.class);
				startActivity(intentListado);
				
			
			}
		});
		hpBtnSolicitar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intentSoli = new Intent(HomePanel.this,SolicitudPanel.class);
				startActivity(intentSoli);

			}

		});
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home_panel, menu);
		return true;
	}

}
