package cl.ufro.service;

import cl.ufro.MuroPanel;
import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Muro;
import cl.ufro.controller.AyudantiaController;
import cl.ufro.controller.InteraccionController;
import cl.ufro.controller.MuroController;
import utils.Notificacion;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

///////////////////////////////
//////El servicio se inicia cuando 
//se presiona el boton ver ayudantias
//se pretende que se inicie al entrar al home(falta por hacer).
///////////////////////////////

public class MyService extends Service {

	MyTask myTask = null;
	Notificacion nt = null;

	@Override
	public void onCreate() {
		super.onCreate();
		Log.e("MSN", "Servicio creado!!");
		myTask = new MyTask();
		nt = new Notificacion(this);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if(myTask.getStatus().ordinal()!=Status.RUNNING.ordinal())
			myTask.execute();
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.e("MSN", "Servicio destruido!!");
		if(!myTask.isCancelled())
			myTask.cancel(true);
	}

	@Override
	public IBinder onBind(Intent intent) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	private class MyTask extends AsyncTask<String, String, String> {

		private boolean cent;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			cent = true;
		}

		@Override
		protected String doInBackground(String... params) {

			while (cent){
				try {
					publishProgress();
					// Stop 5s
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			//llamar al server
			//nt.displayNotification(1, "corto", "titulo", "descripcion");
			AyudantiaController ayudantiaController = new AyudantiaController();
			Ayudantia ayudantia = new Ayudantia();

			MuroController muroController = new MuroController();
			Muro muro = new Muro();

			InteraccionController interaccionController = new InteraccionController();
			if(interaccionController.funcionNotificar()==1)
				nt.displayNotification(1, "Nuevo", "Mensaje", "Podria haber nuevos mensajes");
			 
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			cent = false;
		}
	}
}
