package cl.ufro.dao;

import java.util.List;

import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Usuario;
import utils.ObjetoBd;
import utils.ObjetoDao;

public class AyudantiaDao extends ObjetoDao {
	public Ayudantia find (Ayudantia registro){
		return (Ayudantia)super.find(registro);
	}
	public Ayudantia findById(Ayudantia ayudantia){
		List<ObjetoBd> lista = listAll();
		for(int i=0;i<lista.size();i++){
			Ayudantia aux = (Ayudantia)lista.get(i);
			if( aux.getId()== ayudantia.getId())
				return aux;
		}
		return null;
		//if( aux.getCorreoUsuario().equals(ayudantia.getCorreoUsuario()))
	}
	public Ayudantia findFirst(){
		List<ObjetoBd> lista = listAll();//los elementos de lista van a ser del tipo objetoBd 
		if(lista.size()>0){
			return (Ayudantia)(lista.get(0));//retorna el primero de la lista
		}else 		
		return null;
	}
}
