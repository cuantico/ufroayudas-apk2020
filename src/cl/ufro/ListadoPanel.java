package cl.ufro;

import java.util.ArrayList;
import java.util.List;

import utils.ManagerSession;

import cl.ufro.bd.Asignatura;
import cl.ufro.bd.Ayudantia;
import cl.ufro.controller.AsignaturaController;
import cl.ufro.controller.AyudantiaController;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

public class ListadoPanel extends Activity {
	private ListView listView;
	private ItemAdapter adapter;
	List<Ayudantia> aLista;
	private String lipMateriaSelect;
	private List<Item> items;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listado_panel);
		this.listView = (ListView) findViewById(R.id.listView);

		/*init*/
		AsignaturaController asignaturaController = new AsignaturaController();
		List <Asignatura> lista = asignaturaController.listar();

		String []asignaturas= new String[lista.size()];

		for(int i=0; i<lista.size();i++){
			Asignatura asignatura = lista.get(i);

			String asignaturaServer=asignatura.getNombre();

			asignaturas[i]=asignaturaServer;
		}
		Spinner lipSpinnerAsignatura=(Spinner)findViewById(R.id.lipSpinnerAsignatura);
		lipSpinnerAsignatura.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, asignaturas));
		
		lipSpinnerAsignatura.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parent, View view,int pos, long id) {
				lipMateriaSelect= (String)parent.getItemAtPosition(pos);
				Toast.makeText(parent.getContext(), "Seleccionado:"+lipMateriaSelect, Toast.LENGTH_SHORT).show();	
				
				items = new ArrayList<Item>();

				AyudantiaController ayudantiaController = new AyudantiaController();
				Ayudantia ayudantiaFiltrar = new Ayudantia();
				ayudantiaFiltrar.setMateria(lipMateriaSelect);
				aLista= ayudantiaController.listar(ayudantiaFiltrar);//por Materia

				for(int i=0;i<aLista.size();i++){
					Ayudantia ayudantia = new Ayudantia();
					ayudantia = aLista.get(i);
					if(ayudantia == null){
						break;
					}
					String fecha=ayudantia.getFecha();
					String precio=ayudantia.getOferta();
					String NumPersonas=ayudantia.getnPersonas();
					int idAyudantia = ayudantia.getId();
					items.add(new Item(fecha,precio,NumPersonas,idAyudantia));


				}

				adapter =  new ItemAdapter(parent.getContext(), items);
				listView.setAdapter(adapter);


			}
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.listado_panel, menu);
		return true;
	}

}
