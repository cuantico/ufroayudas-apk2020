package cl.ufro;

import utils.ManagerSession;
import cl.ufro.bd.Usuario;
import cl.ufro.controller.UsuarioController;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class PinPanel extends Activity {
	private EditText ppTxtPin;
	private Button ppBtnIngresar;
	private Usuario usuarioSession;
	private Usuario user; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pin_panel);

		ppTxtPin = (EditText)findViewById(R.id.ppTxtPin);
		ppBtnIngresar = (Button)findViewById(R.id.ppBtnIngresar);
		init();
		ppBtnIngresar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0){
				user.setPin(ppTxtPin.getText().toString());
				UsuarioController usuarioController = new UsuarioController();
				user = usuarioController.identificar(user);

				if(user == null){
					ManagerSession.removeObject("usuario");
					Intent intentLogin = new Intent(PinPanel.this,LoginPanel.class);
					startActivity(intentLogin);
					//ver login
				}else{
					ManagerSession.saveObject("usuario",user);
					Intent intentBienvenida = new Intent(PinPanel.this,BienvenidaPanel.class);
					startActivity(intentBienvenida);
					usuarioController.guardar(user);
					//bienvenida
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pin_panel, menu);
		return true;
	}
	public void init(){
		UsuarioController usuarioController = new UsuarioController();
		usuarioSession = usuarioController.getUsuarioSession();
		user = new Usuario();
		user.setCorreo(usuarioSession.getCorreo());

	}

}
