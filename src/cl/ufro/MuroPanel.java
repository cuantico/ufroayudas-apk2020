package cl.ufro;

import java.util.List;

import utils.Datos;
import utils.ManagerSession;
import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Interaccion;
import cl.ufro.bd.Muro;
import cl.ufro.bd.Usuario;
import cl.ufro.controller.AyudantiaController;
import cl.ufro.controller.InteraccionController;
import cl.ufro.controller.MuroController;
import cl.ufro.controller.UsuarioController;
import cl.ufro.service.MyService;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MuroPanel extends Activity {
	private EditText mpTxtFecha,mpTxtPrecio,mpTxtContenidos,mpTxtMuro,mpTxtAyudante,mpTxtMensaje,mpTxtMateria;
	private Button mpBtnEnviar,mpBtnPactar,mpBtnVolver;
	private TextView mplblEstado;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_muro_panel);
		mpTxtFecha=(EditText) findViewById(R.id.mpTxtFecha);
		mpTxtPrecio=(EditText) findViewById(R.id.mpTxtPrecio);
		mpTxtContenidos=(EditText) findViewById(R.id.mpTxtContenidos);
		mpTxtAyudante=(EditText) findViewById(R.id.mpTxtAyudante);
		mpTxtMensaje=(EditText) findViewById(R.id.mpTxtMensaje);
		mpTxtMuro=(EditText) findViewById(R.id.mpTxtMuro);
		mpBtnEnviar=(Button) findViewById(R.id.mpBtnEnviar);
		mpBtnPactar= (Button) findViewById(R.id.mpBtnPactar);
		mpTxtMateria=(EditText) findViewById(R.id.mpTxtMateria);
		mplblEstado= (TextView) findViewById(R.id.mplblEstado);
		mpBtnVolver = (Button) findViewById(R.id.mpBtnVolver);

		init();stopService(new Intent(MuroPanel.this, MyService.class));
		mpBtnEnviar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				UsuarioController usuController = new UsuarioController();
				MuroController muroController = new MuroController();
				AyudantiaController ayudantiaController = new AyudantiaController();
				InteraccionController interaccionController = new InteraccionController();
				//////////////////////////////////////////////////////////////////////////
				Usuario usuario  = usuController.getUsuarioSession();
				Datos datos = (Datos)ManagerSession.findObject("objeto");
				Ayudantia ayudantiaMuro = new Ayudantia();
				ayudantiaMuro.setId(datos.cifra);
				ayudantiaMuro = ayudantiaController.identificar(ayudantiaMuro);
				String name = usuario.getNombre();
				String msj = name+":"+mpTxtMensaje.getText();
				Muro muro = new Muro();

				muro.setMensaje(msj);
				muro.setCorreo(usuario.getCorreo());
				muro.setIdAyudantia(ayudantiaMuro.getId());

				muro= muroController.actualizar(muro);
				ayudantiaMuro.setIdLastMsj(muro.getIdMsj());				
				ayudantiaController.actualizar(ayudantiaMuro);
				
				Interaccion interaccion = new Interaccion();
				interaccion.setIdMyMsj(muro.getIdMsj());
				interaccion.setIdAyudantia(ayudantiaMuro.getId());
				interaccionController.guardar(interaccion);//local
				//stopService(new Intent(MuroPanel.this, MyService.class));
				Intent actualizar  = new Intent(MuroPanel.this, MuroPanel.class);
				startActivity(actualizar);
			}
		});
		
		mpBtnPactar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				UsuarioController usuControllerPactar = new UsuarioController();
		
				AyudantiaController ayudantiaControllerPactar = new AyudantiaController();
				
				Datos datos2 = (Datos)ManagerSession.findObject("objeto");
				Ayudantia ayudantiaMuroPactar = new Ayudantia();
				ayudantiaMuroPactar.setId(datos2.cifra);
				ayudantiaMuroPactar = ayudantiaControllerPactar.identificar(ayudantiaMuroPactar);
				
				Usuario usuPactar = usuControllerPactar.getUsuarioSession();
				
				if(usuPactar.getCorreo().equals(ayudantiaMuroPactar.getCorreoUsuario())){
					//si es igual es el propietario y confirma ayudante
						if(ayudantiaMuroPactar.getCorreoUsuario()!=null&&ayudantiaMuroPactar.getAyudante()!=null){//mod
						ayudantiaMuroPactar.setConfirmar(true);
						
						Toast.makeText(MuroPanel.this,"Ayudantia Pactada con:"+ayudantiaMuroPactar.getAyudante(), Toast.LENGTH_SHORT).show();
						}
						else{
							Toast.makeText(MuroPanel.this,"No hay ayudante a�n :(", Toast.LENGTH_SHORT).show();
							
						}
				}else{
					//si no es propietario, es ayudante interesado.
					ayudantiaMuroPactar.setAyudante(usuPactar.getCorreo());	
					mpTxtAyudante.setText(""+ayudantiaMuroPactar.getAyudante());
					Toast.makeText(MuroPanel.this,"Ahora eres el ayudante! Exito :D", Toast.LENGTH_SHORT).show();
					Toast.makeText(MuroPanel.this,"La ayudantia se pactara cuando el alumno te acepte como ayudante.", Toast.LENGTH_LONG).show();
				}
				
			    ayudantiaControllerPactar.actualizar(ayudantiaMuroPactar);
			
			}
		});
		mpBtnVolver.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent	volver = new Intent(MuroPanel.this, HomePanel.class);
				startActivity(volver);
			}
		});;

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.muro_panel, menu);
		return true;
	}
	public void init(){
		Datos datos = (Datos)ManagerSession.findObject("objeto");
		int id = datos.cifra;
		//Toast.makeText(MuroPanel.this,"AID:"+id, Toast.LENGTH_SHORT).show();

		Ayudantia ayudantiaMuro = new Ayudantia();
		ayudantiaMuro.setId(id);

		AyudantiaController ayudantiaController = new AyudantiaController();
		ayudantiaMuro = ayudantiaController.identificar(ayudantiaMuro);
		//Rellenar campos

		mpTxtContenidos.setText(ayudantiaMuro.getContenidos());
		mpTxtFecha.setText(ayudantiaMuro.getFecha());;
		mpTxtPrecio.setText(ayudantiaMuro.getOferta());
		mpTxtMateria.setText(ayudantiaMuro.getMateria());
		if(ayudantiaMuro.isConfirmar()==true){
			mplblEstado.setText("Ayudantia Pactada con: "+ayudantiaMuro.getAyudante());
		}
		if(ayudantiaMuro.getAyudante()!=null)
			mpTxtAyudante.setText(""+ayudantiaMuro.getAyudante());
		else
			mpTxtAyudante.setText("");
		
		Muro muro = new Muro();
		muro.setIdAyudantia(ayudantiaMuro.getId());
		MuroController muroController = new MuroController();
		List<Muro> lista = muroController.ListarMuro(ayudantiaMuro);
		String msj = "";
		for(int i= 0;i<lista.size();i++){
			Muro m = lista.get(i);
			msj = msj+m.getMensaje()+"\n";			
		}
		mpTxtMuro.setText(msj);

	}

}
