package cl.ufro;

import java.util.List;

import cl.ufro.service.MyService;




import utils.Datos;
import utils.ManagerSession;
import utils.Notificacion;
import android.R.string;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

public class ItemAdapter extends BaseAdapter {
	private Context context;
	private List<Item> items;
    private Notificacion noti;
 

	public ItemAdapter(Context context, List<Item> items) {
		this.context = context;
		this.items = items;
		 noti = new Notificacion(context);
	}
	@Override
	public int getCount() {
		return this.items.size();
	}

	@Override
	public Object getItem(int position) {
		return this.items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		View rowView = convertView;

		if (convertView == null) {
			// Create a new view into the list.
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.activity_item_lista_ayudantia_panel, parent, false);
		}

		Item item = this.items.get(position);
		// Set data into the view.
		TextView tvFecha = (TextView) rowView.findViewById(R.id.ilapFecha);
		tvFecha.setText(item.getFecha());
		TextView tvPrecio = (TextView) rowView.findViewById(R.id.ilapPrecio);
		tvPrecio.setText(item.getPrecio());
		TextView tvNumPersonas = (TextView) rowView.findViewById(R.id.ilapNumPersonas);
		tvNumPersonas.setText(item.getNumPersonas());
		Button btnEntrar = (Button) rowView.findViewById(R.id.ilapBtnEntrar);
		btnEntrar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {

				Item item = items.get(position);
				int id = item.getId();//obtenemos el id de la ayudantia selec
				Intent intent  = new Intent(context, MuroPanel.class);
				Datos datos = new Datos();
				datos.cifra=id;
				ManagerSession.saveObject("objeto", datos);
		
				context.startActivity(intent);
				
			}
		});



		return rowView;
	}
}
