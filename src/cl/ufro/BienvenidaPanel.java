package cl.ufro;

import cl.ufro.bd.Usuario;
import cl.ufro.controller.UsuarioController;
import cl.ufro.service.MyService;
import utils.ManagerSession;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class BienvenidaPanel extends Activity {
	private Button bpBtnGuardar;
	private EditText bpTxtNombre;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bienvenida_panel);
		bpBtnGuardar = (Button) findViewById(R.id.bpBtnGuardar);
		bpTxtNombre = (EditText) findViewById(R.id.bpTxtNombre);
		Log.e("MSN", "hola");
		bpBtnGuardar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				UsuarioController uController = new UsuarioController();
				Usuario usuarioNombre = uController.getUsuarioSession();
				usuarioNombre.setNombre(bpTxtNombre.getText().toString());
				uController.actualizar(usuarioNombre);
				
				
			
				
				
				ManagerSession.saveObject("usuario", usuarioNombre);
				Intent intentHome = new Intent(BienvenidaPanel.this,HomePanel.class);
				startActivity(intentHome);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.bienvenida_panel, menu);
		return true;
	}

}
