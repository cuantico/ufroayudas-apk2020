package utils;


import cl.ufro.NotificacionView;
import cl.ufro.R;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class Notificacion {
	private Context context;
	
	public Notificacion(Context context){
		this.context = context;
	}

	public void displayNotification(int notificationID, String mensajeCorto, String titulo, String Descripcion){
		//Context applicationContext = activity.getApplicationContext();
		Intent i = new Intent(context, NotificacionView.class);
		i.putExtra("notificationID", notificationID);

		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, i, 0);
		NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

		CharSequence ticker =mensajeCorto;
		CharSequence contentTitle = titulo;
		CharSequence contentText = Descripcion;
		Notification noti = new NotificationCompat.Builder(context)
		.setContentIntent(pendingIntent)
		.setTicker(ticker)
		.setContentTitle(contentTitle)
		.setContentText(contentText)
		.setSmallIcon(R.drawable.ic_launcher)
		.addAction(R.drawable.ic_launcher, ticker, pendingIntent)
		.setVibrate(new long[] {100, 250, 100, 500})
		.build();
		nm.notify(notificationID, noti);
	}
}
